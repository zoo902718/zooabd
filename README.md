#Projet Zoo, Zoo ABD

##Ce projet a été réaliser par Agueni Yanis, Bernal Pierre et Diarra Maimouna
(D'ou le ,nom Zoo ABD)

##Pour ajouter cloner

Via tortoise Git, créer un dépot avec la clé SSH git@gitlab.com:zoo902718/zooabd.git
Ou avec le Https : https://gitlab.com/zoo902718/zooabd.git
Sinon télécharger le projet en .zip et importer le dans votre éclipse


##Pour ajouter un fichier

```
cd existing_repo
git remote add origin https://gitlab.com/zoo902718/zooabd.git
git branch -M main
git push -uf origin main
```

##Les languages et logiciels utilisés

- Eclipse
- Java
- Swing (pour l'affichage)

##Les documents

Les documents tel que la JavaDoc ou bien le manuel d'utilisation sont directement dans la racine du projet