package controller;

import models.Animal.Animal;
import models.Animal.Mammal.*;
import models.Animal.Other.Eagle;
import models.Animal.Other.Goldfish;
import models.Animal.Other.Penguin;
import models.Enclosure.Aquarium;
import models.Enclosure.Aviary;
import models.Enclosure.Enclosure;
import models.Enclosure.StandardEnclosure;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EnclosureController {

    public static void chooseAnAction(String enclosure) {

        Scanner sc = new Scanner(System.in);
        int choice = sc.nextInt();

        // AListe des animaux
        List<Animal> wolfList = new ArrayList<>();
        wolfList.add(new Wolf("Garou", "M�le", 26.44, 1.55, false, false, true, (byte) 69));

        List<Animal> tigerList = new ArrayList<>();
        tigerList.add(new Tiger("Pierre", "M�le", 56.44, 1.85, true, false, true, (byte) 102));

        List<Animal> bearList = new ArrayList<>();
        bearList.add(new Bear("Lucas", "M�le", 155.53, 2.65, true, false, true, (byte) 225));

        List<Animal> whaleList = new ArrayList<>();
        whaleList.add(new Whale("Thierry", "Femelle", 27000.49, 11.55, false, false, true, (byte) 365));

        List<Animal> goldfishList = new ArrayList<>();
        goldfishList.add(new Goldfish("N�mo", "M�le", 0.11, 0.48, true, false, true, (byte) 5));

        List<Animal> sharkList = new ArrayList<>();
        sharkList.add(new Shark("CroqueMonsieur", "M�le", 140.32, 3.12, true, false, true, (byte) 274));

        List<Animal> eagleList = new ArrayList<>();
        eagleList.add(new Eagle("Desert", "Femelle", 5.34, 0.98, false, false, true, (byte) 45));

        List<Animal> penguinList = new ArrayList<>();
        penguinList.add(new Penguin("Mario", "M�le", 0.72, 0.43, false, true, true, (byte) 37));

        // Enclos
        StandardEnclosure enclosLoup = new StandardEnclosure("Enclos des loups", 950, 15, 1, (ArrayList<Animal>) wolfList, (byte) 4, "Loup");
        StandardEnclosure enclosTigre = new StandardEnclosure("Enclos des tigres", 1200, 12, 1, (ArrayList<Animal>) tigerList, (byte) 7, "Tigre");
        StandardEnclosure enclosOurs = new StandardEnclosure("Enclos des ours", 1280, 7, 1, (ArrayList<Animal>) bearList, (byte) 5, "Ours");
        Aquarium enclosBaleine = new Aquarium("Enclos des baleines", 12000, 5, 1, (ArrayList<Animal>) whaleList, (byte) 10, "Baleine", 500, 3);
        Aquarium enclosPoissonRouge = new Aquarium("Enclos des poissons rouges", 500, 40, 1, (ArrayList<Animal>) goldfishList, (byte) 10, "Poisson rouge", 15, 3);
        Aquarium enclosRequin = new Aquarium("Enclos des requins", 7000, 10, 1, (ArrayList<Animal>) sharkList, (byte) 7, "Requin", 200, 3);
        Aviary enclosAigle = new Aviary("Enclos d'aigles", 5000, 10, 1, (ArrayList<Animal>) eagleList, (byte) 5, "Aigle", 100);
        StandardEnclosure enclosPinguin = new StandardEnclosure("Enclos de pingouins", 800, 40, 1, (ArrayList<Animal>) penguinList, (byte) 9, "Pinguin");

        // Animaux � ajouter dans les enclos
        Wolf loup2 = new Wolf("Gazelle", "Femelle", 26.44, 1.55, false, false, true, (byte) 69);
        Tiger tigre2 = new Tiger("Pierrette", "Femelle", 56.44, 1.85, true, false, true, (byte) 102);
        Bear ours2 = new Bear("Shassi", "Femelle", 155.53, 2.65, true, false, true, (byte) 225);
        Whale baleine2 = new Whale("Pascale", "M�le", 27000.49, 11.55, false, false, true, (byte) 365);
        Goldfish poisson2 = new Goldfish("Dory", "Femelle", 0.11, 0.48, true, false, true, (byte) 5);
        Shark requin2 = new Shark("CroqueMadame", "Femelle", 140.32, 3.12, true, false, true, (byte) 274);
        Eagle aigle2 = new Eagle("Daniel", "M�le", 5.34, 0.98, false, false, true, (byte) 45);
        Penguin pingouin2 = new Penguin("Pingu", "M�le", 0.72, 0.43, false, true, true, (byte) 37);


        switch (choice) {
            case 1:
                switch(enclosure) {
                        case "Enclos des loups":
                            enclosLoup.displayEnclosureAndAnimalsCharacteristics(enclosLoup);
                            break;
                        case "Enclos des tigres":
                            enclosTigre.displayEnclosureAndAnimalsCharacteristics(enclosTigre);
                            break;
                        case "Enclos des ours":
                            enclosOurs.displayEnclosureAndAnimalsCharacteristics(enclosOurs);
                            break;
                        case "Enclos des baleines":
                            enclosBaleine.displayEnclosureAndAnimalsCharacteristics(enclosBaleine);
                            break;
                        case "Enclos des poissons rouges":
                            enclosPoissonRouge.displayEnclosureAndAnimalsCharacteristics(enclosPoissonRouge);
                            break;
                        case "Enclos des requins":
                            enclosRequin.displayEnclosureAndAnimalsCharacteristics(enclosRequin);
                            break;
                        case "Enclos des aigles":
                            enclosAigle.displayEnclosureAndAnimalsCharacteristics(enclosAigle);
                            break;
                        case "Enclos des pingouins":
                            enclosPinguin.displayEnclosureAndAnimalsCharacteristics(enclosPinguin);
                            break;
                }
                break;
            case 2:
                switch(enclosure) {
                    case "Enclos des loups":
                        enclosLoup.addAnimal(loup2, enclosLoup);
                        break;
                    case "Enclos des tigres":
                        enclosTigre.addAnimal(tigre2, enclosTigre);
                        break;
                    case "Enclos des ours":
                        enclosOurs.addAnimal(ours2, enclosOurs);
                        break;
                    case "Enclos des baleines":
                        enclosBaleine.addAnimal(baleine2, enclosBaleine);
                        break;
                    case "Enclos des poissons rouges":
                        enclosPoissonRouge.addAnimal(poisson2, enclosPoissonRouge);
                        break;
                    case "Enclos des requins":
                        enclosRequin.addAnimal(requin2, enclosRequin);
                        break;
                    case "Enclos des aigles":
                        enclosAigle.addAnimal(aigle2, enclosAigle);
                        break;
                    case "Enclos des pingouins":
                        enclosPinguin.addAnimal(pingouin2, enclosPinguin);
                        break;
                }
                break;
            case 3:
                switch(enclosure) {
                    case "Enclos des loups":
                        enclosLoup.removeAnimal(loup2, enclosLoup);
                        break;
                    case "Enclos des tigres":
                        enclosTigre.removeAnimal(tigre2, enclosTigre);
                        break;
                    case "Enclos des ours":
                        enclosOurs.removeAnimal(ours2, enclosOurs);
                        break;
                    case "Enclos des baleines":
                        enclosBaleine.removeAnimal(baleine2, enclosBaleine);
                        break;
                    case "Enclos des poissons rouges":
                        enclosPoissonRouge.removeAnimal(poisson2, enclosPoissonRouge);
                        break;
                    case "Enclos des requins":
                        enclosRequin.removeAnimal(requin2, enclosRequin);
                        break;
                    case "Enclos des aigles":
                        enclosAigle.removeAnimal(aigle2, enclosAigle);
                        break;
                    case "Enclos des pingouins":
                        enclosPinguin.removeAnimal(pingouin2, enclosPinguin);
                        break;
                }
                break;
            case 4:
                switch(enclosure) {
                    case "Enclos des loups":
                        enclosLoup.feedAnimals(enclosLoup);
                        break;
                    case "Enclos des tigres":
                        enclosTigre.feedAnimals(enclosTigre);
                        break;
                    case "Enclos des ours":
                        enclosOurs.feedAnimals(enclosOurs);
                        break;
                    case "Enclos des baleines":
                        enclosBaleine.feedAnimals(enclosBaleine);
                        break;
                    case "Enclos des poissons rouges":
                        enclosPoissonRouge.feedAnimals(enclosPoissonRouge);
                        break;
                    case "Enclos des requins":
                        enclosRequin.feedAnimals(enclosRequin);
                        break;
                    case "Enclos des aigles":
                        enclosAigle.feedAnimals(enclosAigle);
                        break;
                    case "Enclos des pingouins":
                        enclosPinguin.feedAnimals(enclosPinguin);
                        break;
                }
                break;
            case 5:
                switch(enclosure) {
                    case "Enclos des loups":
                        enclosLoup.cleaning(enclosLoup);
                        break;
                    case "Enclos des tigres":
                        enclosTigre.cleaning(enclosTigre);
                        break;
                    case "Enclos des ours":
                        enclosOurs.cleaning(enclosOurs);
                        break;
                    case "Enclos des baleines":
                        enclosBaleine.cleaning(enclosBaleine);
                        break;
                    case "Enclos des poissons rouges":
                        enclosPoissonRouge.cleaning(enclosPoissonRouge);
                        break;
                    case "Enclos des requins":
                        enclosRequin.cleaning(enclosRequin);
                        break;
                    case "Enclos des aigles":
                        enclosAigle.cleaning(enclosAigle);
                        break;
                    case "Enclos des pingouins":
                        enclosPinguin.cleaning(enclosPinguin);
                        break;
                }
                break;
            case 6:
                System.out.println("Voulez-vous quitter le programme ? (O/N)");
                String answer = sc.nextLine();
                if(answer.equals("O") || answer.equals("o")) {
                    System.out.println("� la prochaine !");
                    break;
                }
                else {
                    System.out.println("Veuillez choisir une option");
                }
        }
    }
}
